#!/usr/bin/env python

import unittest, time

def getTriangle(filename):
    with open(filename) as f:
        content = f.read().splitlines()
    return [map(int, line.rstrip('\n ').split(' ')) for line in content]

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print '\n%r took %2.2f ms to run \n' % \
                  (method.__name__, (te - ts) * 1000)
        return result
    return timed

def maxSum(triangle): 
    # Analyze the problem and see the order in which the sub-problems are solved and start solving from the trivial subproblem, up towards the given problem. 
    for i in range(len(triangle)-2, -1, -1): 
        for j in range(i+1):
            # Dynamic Optimization: Starting at the bottom, choose the greatest path upwards
            # Choose the biggest item then move on
            if (triangle[i+1][j] > triangle[i+1][j+1]):
                triangle[i][j] += triangle[i+1][j] 
            else:
                triangle[i][j] += triangle[i+1][j+1]

    # Now were here, so return the sum after problem is solved 
    return triangle[0][0]

class TestTriangle(unittest.TestCase):
    @timeit
    def testSimpleTriangle(self):
        self.assertEqual(maxSum(getTriangle('triangle.sample.txt')), 27)
    
    @timeit
    def testJtTriangle(self):
        self.assertEqual(maxSum(getTriangle('triangle.real.txt')), 732506)

if __name__ == '__main__':
    unittest.main()

